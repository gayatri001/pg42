#include<stdio.h>
int input()
{
    int p;
    printf("enter a number with three digits\n");
    scanf("%d",&p);
    return p;
}
int compute(int p)
{
    int a,b,c,sum;
    a=p/100;
    b=p%10;
    c=(p%100)/10;
    sum=a+b+c;
    return sum;
}
void output(int p,int sum)
{
    int a,b,c;
    a=p/100;
    b=p%10;
    c=(p%100)/10;
    printf("the sum of %d,%d and %d is %d\n",a,c,b,sum);
}
int main()
{
    int p,sum;
    p=input();
    sum=compute(p);
    output(p,sum);
    return 0;
}