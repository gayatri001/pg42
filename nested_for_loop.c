#include<stdio.h>
int input ()
{
  int n;
  printf ("the number of digits you want\n");
  scanf ("%d", &n);
  return n;
}

void output (int n)
{
  for (int i = 1; i <=n; i++)
    {
      for (int j = 1; j <=i; j++)
	{
	  printf ("%d",j);
	}
      printf ("\n");
    }
}

int
main ()
{
  int n;
  n = input ();
  output (n);
  return 0;
}
