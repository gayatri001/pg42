#include<stdio.h>
int input()
{
    int n;
    printf("enter a number\n");
    scanf("%d",&n);
    return n;
}
int compute1(int n)
{
    int rem;
    int r=0;
    while(n!=0)
    {
        rem=n%2;
        n=n/2;
        r++;
    }
    return r;
}
void compute2(int n,int r,int a[r])
{
    while(n!=0)
    {
        for(int i=0;i<r;i++)
        {
            a[i]=n%2;
        }
        n=n/2;
    }
}
void output(int r,int a[r])
{
    printf("the decimal number in binary form \n");
    for(int i=0;i<r;i++)
    {
        printf("%d",&a[i]);
        printf("\t");
    }
}
int main()
{
    int n,r;
    n=input();
    r=compute1(n);
    int a[r];
    compute2(n,r,a);
    output(r,a);
    return 0;
    
}