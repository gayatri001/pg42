
#include<stdio.h>
struct fractions
{
    int x;
    int y;
};
typedef struct fractions frac;
frac input()
{
    frac p;
    printf("enter the numerator and denominator\n");
    scanf("%d %d",&p.x,&p.y);
    return p;
}
frac compute(frac v,frac b)
{
    frac e;
    e.x=v.x*b.y+b.x*v.y;
    e.y=v.y*b.y;
    return e;
}
void output(frac t,frac i,frac l)
{
    printf("the addition of %d/%d and %d/%d is %d/%d\n",t.x,t.y,i.x,i.y,l.x,l.y);
}
int main()
{
    frac g;
    frac h;
    frac o;
    g=input();
    h=input();
    o=compute(g,h);
    output(g,h,o);
    return 0;
}