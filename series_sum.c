#include<stdio.h>
#include<math.h>
int input()
{
    int p;
    printf("enter a number\n");
    scanf("%d",&p);
    return p;
}
int compute(int n)
{
    int sum=0;
    for(int i=1;i<=n;i++)
    {
        int sq=pow(i,2);
        sum=sum+sq;
    }
    return sum;
}
void output(int num,int sum)
{
    printf("1^2+2^2+3^2+4^2+....+%d^2\n is %d",num,sum);
}
int main()
{
    int p,sum;
    p=input();
    sum=compute(p);
    output(p,sum);
    return 0;
}