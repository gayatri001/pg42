#include<stdio.h>

struct complex

{

    int x;

    int y;

};

typedef struct complex comp;

comp input()

{

    comp p;

    printf("enter real and imaginary part");

    scanf("%d %d",&p.x,&p.y);

    return p;

}

comp compute(comp a,comp b)

{

    comp c;

    c.x=a.x+b.x;

    c.y=a.y+b.y;

    return c;

}

void output(comp p,comp q,comp r)

{

    printf("the sum of %d+i%d and %d+i%d is %d+i%d",p.x,p.y,q.x,q.y,r.x,r.y);

}

int main()

{

    comp g;

    comp h;

    comp j;

    g=input();

    h=input();

    j=compute(g,h);

    output(g,h,j);

    return 0;

}