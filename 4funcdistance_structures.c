#include<stdio.h>
#include<math.h>
struct distance
{
    float x;
    float y;
};
typedef struct distance dis;
dis input()
{
    dis p;
    printf("enter both x and y coordinates\n");
    scanf("%f %f",&p.x,&p.y);
    return p;
}
float compute(dis p,dis o)
{
    float l;
    l=sqrt((pow (p.x-o.x,2))+(pow (p.y-o.y,2)));
    return l;
}
void output(dis p,dis o,float l)
{
    printf("The distance between the points %f,%f and %f,%f is %f\n",p.x,p.y,o.x,o.y,l);
}
int main()
{
    dis g;
    dis k;
    float l;
    g=input();
    k=input();
    l=compute(g,k);
    output(g,k,l);
    return 0;
}