#include<stdio.h>

void input_string( char str[100])
{
	printf("enter the string\n");
	scanf("%s",str);
}
char input_char()
{
	char c;
	printf("enter the character whose frequency you want to find\n");
	scanf("%c",&c);
	return c;
}
int compute_frequency(char str[100],char c)
{
	int k=0;
	for(int i=0;str[i]!='\0';i++)
	{
		if(c==str[i])
		{
			k++;
		}
	}
	return k;
}
void output(char str[100],char c,int k)
{
	printf("the string you have entered is %s\n",str);
	printf("the character you have entered is:\n");
	putchar(c);
	if(k==0)
	{
		printf("the given character is not present in the string\n");
	}
	else
	{
		printf("the given character has repeated %d times in the string\n",k);
	}
}
int main()
{
	char c;
	int k;
	char str[100];
	input_string(str);
	c=input_char();
	k=compute_frequency(str,c);
	output(str,c,k);
	return 0;
}
	
	
