#include<stdio.h>
#include<math.h>
struct time
{
    int hr;
    int min;
};
typedef struct time time;

time input()
{
    time p;
    printf("enter time in hours:mins\n");
    scanf("%d:%d",&p.hr,&p.min);
    return p;
}
int compute(time r)
{
    int t;
    t=r.hr*60+r.min;
    return t;
}
void output(time n,int t)
{
    printf("you entered the time is %d:%d\n",n.hr,n.min);
    printf("the time in minutes is %d\n",t);
}
int main()
{
    time n;
    int t;
    n=input();
    t=compute(n);
    output(n,t);
    return 0;
}