#include<stdio.h>
int input()
{
    int n;
    printf("enter a number\n");
    scanf("%d",&n);
    return n;
}
void swap(int *a,int *b)
{
    int temp;
    temp=*b;
    *b=*a;
    *a=temp;
}
void output(int a,int b)
{
    printf("the numbers you entered are %d and %d\n",a,b);
    printf("the numbers after swapping are %d and %d\n",b,a);
}
int main()
{
    int a,b;
    a=input();
    b=input();
    swap(&a,&b);
    output(a,b);
    return 0;
}