#include<stdio.h>
int input()
{
    int p;
    printf("enter a number\n");
    scanf("%d",&p);
    return p;
}
int compute(int num)
{
    int sum=0;
    int d;
    while(num!=0)
    {
        d=num%10;
        sum=sum+d;
        num=num/10;
    }
    return sum;
}
void output(int num,int sum)
{
    printf("the sum of the digits of %d is %d\n",num,sum);
}
int main()
{
    int p,sum;
    p=input();
    sum=compute(p);
    output(p,sum);
    return 0;
}