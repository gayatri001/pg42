#include<stdio.h>
struct book
{
    char name[100];
    int number;
};
struct library
{
    char name_student[100];
    char branch_student[100];
    struct book b;
};
typedef struct library lib;
lib input()
{
    lib e;
    printf("enter the name of the student\n");
    scanf("%s",e.name_student);
    printf("enter the branch  of student\n");
    scanf("%s",e.branch_student);
    printf("enter the book name\n");
    scanf("%s",e.b.name);
    printf("enter the book number\n");
    scanf("%d",&e.b.number);
    return e;
}

void output(lib e)
{
    printf("the name of the student who took the book is %s\n",e.name_student);
    printf("the student's branch is %s\n",e.branch_student);
    printf("the book name is %s\n",e.b.name);
    printf("the book number is %d\n",e.b.number);
}
int main()
{
    lib e;
    e=input();
    output(e);
    return 0;
}