#include<stdio.h>
struct fractions
{
    int num;
    int den;
};
typedef struct fractions frac;
int input()
{
    int n;
    printf("how many fractions to add?\n");
    scanf("%d",&n);
    return n;
}
void input1(int n,frac a[n])
{
    for(int i=0;i<n;i++)
    {
        printf("enter the numerator\n");
        scanf("%d",&a[i].num);
        printf("enter the denominator\n");
        scanf("%d",&a[i].den);
    }
}
int gcd(int m,int n)
{

    int r;
    int temp;
    if(n>m)
    {
        temp=m;
        m=n;
        n=temp;
    }



    while(n!=0)
    {
        r=m%n;
        m=n;
        n=r;
    }
    return m;


}
int reduce(int sum,int lcm)
{
    int g;
    g=gcd(sum,lcm);
    return g;


}
int sum(int n,frac a[n],int lcm)
{
    int sum=0;
    for(int i=0;i<n;i++)
    {
        sum=sum+a[i].num*(lcm/a[i].den);
    }
    return sum;
}
int largest_factor(int n,frac a[n])
{
    int lcm,m,p;
    m=a[0].den;
    for(int i=1;i<n;i++)
    {
        p=a[i].den;
        if(m%p==0)
        {
            lcm=p;
        }
        if(p%m==0)
        {
            lcm=m;
        }
        else
        {
            lcm=p*m;
        }
        m=lcm;
    }
    return lcm;
}
frac simplify(int sum,int lcm,int g)
{
    frac ans;
    ans.num=sum/g;
    ans.den=lcm/g;
    return ans;

}
void output(frac ans)
{
    printf("the sum of the given fractions is %d/%d",ans.num,ans.den);
}

int main()
{
    int n,lcm,g,s;
    n=input();
    frac a[n];
    input1(n,a);
    lcm=largest_factor(n,a);
    s=sum(n,a,lcm);
    g=gcd(sum,lcm);
    frac answer;
    answer=simplify(sum,lcm,g);
    return 0;


}